import unittest
from django.contrib.auth.models import User

from projects.models import Project, Task

from home.templatetags.home_extras import (
    check_nr_pending_offers,
    check_nr_user_offers,
    task_status,
    get_task_statuses,
    get_user_task_statuses)

from test_helpers import (
    delete_test_data,
    create_multiple_test_data,
    create_task_status_data)


class OfferCountingRegressionTest(unittest.TestCase):
    def setUp(self):
        create_multiple_test_data()

    def tearDown(self):
        delete_test_data()

    def test_check_nr_pending_offers(self):
        test_project = Project.objects.get(title="Project1")
        self.assertEquals(check_nr_pending_offers(test_project), 1)

    def test_check_nr_user_offers(self):
        test_project = Project.objects.get(title="Project1")
        test_user = User.objects.get(username="User2")

        result = check_nr_user_offers(test_project, test_user)
        self.assertEquals(result['declined'], 0)
        self.assertEquals(result['pending'], 1)
        self.assertEquals(result['accepted'], 1)


class TaskStatusRegressionTest(unittest.TestCase):
    def setUp(self):
        create_task_status_data()

    def tearDown(self):
        delete_test_data()

    def test_task_pending_acc(self):
        result = task_status(Task.objects.get(title="Task5"))
        self.assertEquals(result, "You have deliveries waiting for acceptance")

    def test_task_pending_pay(self):
        result = task_status(Task.objects.get(title="Task6"))
        self.assertEquals(result, "You have deliveries waiting for payment")

    def test_task_payment_sent(self):
        result = task_status(Task.objects.get(title="Task2"))
        self.assertEquals(result, "You have sent payment")

    def test_task_awaiting_del(self):
        result = task_status(Task.objects.get(title="Task1"))
        self.assertEquals(result, "You are awaiting delivery")


class GetTaskStatusesRegressionTest(unittest.TestCase):
    def setUp(self):
        create_multiple_test_data()

    def tearDown(self):
        delete_test_data()

    def test_get_task_statuses(self):
        test_project = Project.objects.get(title="Project2")

        result = get_task_statuses(test_project)
        self.assertEquals(result['awaiting_delivery'], 2)
        self.assertEquals(result['pending_acceptance'], 0)
        self.assertEquals(result['pending_payment'], 0)
        self.assertEquals(result['payment_sent'], 0)
        self.assertEquals(result['declined_delivery'], 0)

    def test_get_user_task_statuses(self):
        test_project = Project.objects.get(title="Project1")
        test_user = User.objects.get(username="User2")

        result = get_user_task_statuses(test_project, test_user)
        self.assertEquals(result['awaiting_delivery'], 0)
        self.assertEquals(result['pending_acceptance'], 0)
        self.assertEquals(result['pending_payment'], 0)
        self.assertEquals(result['payment_sent'], 1)
        self.assertEquals(result['declined_delivery'], 0)
