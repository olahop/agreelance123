import unittest
from django.test import Client

from test_helpers import create_test_user, delete_test_data


class HomeViewRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_test_user()

    def tearDown(self):
        delete_test_data()

    def test_authenticated_user(self):
        self.client.login(username='User', password='top_secret')
        response = self.client.get('/', follow=True)
        self.assertTrue(('/projects/', 302) not in response.redirect_chain)

    def test_unauthenticated_user(self):
        response = self.client.get('/', follow=True)
        self.assertTrue(('/projects/', 302) in response.redirect_chain)
