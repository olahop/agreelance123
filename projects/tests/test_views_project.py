import unittest
from django.test import Client

from user.models import User
from projects.models import Project, Task, TaskOffer

from test_helpers import delete_test_data, create_multiple_test_data


class ProjectViewStatementTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()
        self.project_id = Project.objects.get(title="Project2").pk

    def tearDown(self):
        delete_test_data()

    def test_get(self):
        self.client.get(f'/projects/{self.project_id}/')

    def test_post_offer_response(self):
        task_offer = TaskOffer.objects.get(title="TaskOffer4")
        self.client.login(username='User2', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/', {
            'offer_response': True,
            'taskofferid': task_offer.id,
            'status': 'a',
            'feedback': 'Feedback',
        })

    def test_post_edit_project(self):
        test_task1 = Task.objects.get(title="Task3")
        test_task2 = Task.objects.get(title="Task4")
        self.client.login(username='User2', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/', {
            'edit_project': True,
            'project_description': 'Edited',
            f'task{test_task1.id}_budget': '100',
            f'task{test_task1.id}_description': 'Edited',
            f'task{test_task2.id}_budget': '20',
            f'task{test_task2.id}_description': 'Edited',
        })

    def test_post_status_change(self):
        self.client.login(username='User2', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/', {
            'status_change': True,
            'status': 'o',
        })

    def test_post_offer_submit(self):
        test_task = Task.objects.get(title="Task4")
        self.client.login(username='User1', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/', {
            'offer_submit': True,
            'title': 'TaskOffer5',
            'description': 'Offer description',
            'price': 567,
            'taskvalue': test_task.id
        })

    def test_post_edit_offer(self):
        task_offer = TaskOffer.objects.get(title="TaskOffer3")
        self.client.login(username='User1', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/', {
            'edit_offer': True,
            'offer_id': task_offer.id,
            'offer_description': 'Edited',
            'offer_price': '30',
        })


class ProjectViewRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()
        self.project_id = Project.objects.get(title="Project2").pk

    def tearDown(self):
        delete_test_data()

    def test_project_owner_request(self):
        self.client.login(username='User2', password='top_secret')
        response = self.client.post(f'/projects/{self.project_id}/')
        self.assertIsNotNone(response.context['project'])
        self.assertIsNotNone(response.context['tasks'])
        self.assertIsNotNone(response.context['status_form'])
        self.assertIsNotNone(response.context['total_budget'])
        self.assertIsNotNone(response.context['offer_response_form'])

    def test_non_project_owner_request(self):
        response = self.client.post(f'/projects/{self.project_id}/')
        self.assertIsNotNone(response.context['project'])
        self.assertIsNotNone(response.context['tasks'])
        self.assertIsNotNone(response.context['total_budget'])
        self.assertIsNotNone(response.context['task_offer_form'])

    def test_post_offer_response(self):
        task_offer = TaskOffer.objects.get(title="TaskOffer4")
        self.client.login(username='User2', password='top_secret')
        response = self.client.post(f'/projects/{self.project_id}/', {
            'offer_response': True,
            'taskofferid': task_offer.id,
            'status': 'a',
            'feedback': 'Feedback',
        })

        test_offerer = User.objects.get(username="User1")
        self.assertTrue(
            test_offerer.profile in response.context['project'].participants.all())
        self.assertTrue(test_offerer.profile in task_offer.task.read.all())
        self.assertTrue(test_offerer.profile in task_offer.task.write.all())

    def test_post_edit_project(self):
        test_task1 = Task.objects.get(title="Task3")
        test_task2 = Task.objects.get(title="Task4")
        self.client.login(username='User2', password='top_secret')
        response = self.client.post(f'/projects/{self.project_id}/', {
            'edit_project': True,
            'project_description': 'Edited',
            f'task{test_task1.id}_budget': '100',
            f'task{test_task1.id}_description': 'Edited',
            f'task{test_task2.id}_budget': '20',
            f'task{test_task2.id}_description': 'Edited',
        })
        self.assertFalse(response.context['project'].edit)
        self.assertEquals(response.context['project'].description, "Edited")
        self.assertEquals(response.context['tasks'][0].budget, "100")
        self.assertEquals(response.context['tasks'][0].description, "Edited")
        self.assertEquals(response.context['tasks'][1].budget, "20")
        self.assertEquals(response.context['tasks'][1].description, "Edited")

    def test_post_offer_submit(self):
        test_task = Task.objects.get(title="Task4")
        self.client.login(username='User1', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/', {
            'offer_submit': True,
            'title': 'TaskOffer5',
            'description': 'Offer description',
            'price': 567,
            'taskvalue': test_task.id
        })

        task_offer = TaskOffer.objects.get(title="TaskOffer5")
        self.assertEquals(task_offer.task, test_task)
        self.assertEquals(task_offer.description, "Offer description")
        self.assertEquals(task_offer.price, 567)
        self.assertEquals(task_offer.offerer.user.username, "User1")

    def test_post_edit_offer(self):
        task_offer = TaskOffer.objects.get(title="TaskOffer3")
        self.client.login(username='User1', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/', {
            'edit_offer': True,
            'offer_id': task_offer.id,
            'offer_description': 'Edited',
            'offer_price': '30',
        })

        task_offer = TaskOffer.objects.get(title="TaskOffer3")
        self.assertEquals(task_offer.description, "Edited")
        self.assertEquals(task_offer.price, 30)
        self.assertFalse(task_offer.edit)


class AcceptingOfferOutputTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()
        self.project = Project.objects.get(title="Project1")

    def tearDown(self):
        delete_test_data()

    def test_post_offer_accept(self):
        self.client.login(username='User1', password='top_secret')
        response = self.client.post(f'/projects/{self.project.pk}/', {
            'offer_response': True,
            'taskofferid': TaskOffer.objects.get(title="TaskOffer2").id,
            'status': 'a',
            'feedback': 'feedback',
        })

        task = Task.objects.get(title="Task2")
        task_offer = TaskOffer.objects.get(title="TaskOffer2")
        offerer = User.objects.get(username="User2")

        # Checks output project relevant data
        self.assertEqual(response.context['project'], self.project)
        self.assertTrue(
            offerer.profile in response.context['project'].participants.all())
        # self.assertEqual(project.status, 'i')

        # Checks output task relevant data
        self.assertTrue(task in response.context['tasks'])
        self.assertEqual(task.accepted_task_offer(), task_offer)
        self.assertTrue(offerer.profile in task.read.all())
        self.assertTrue(offerer.profile in task.write.all())
        self.assertFalse(offerer.profile in task.modify.all())

        # Checks output offer relevant data
        self.assertEqual(task_offer.status, 'a')
        self.assertEqual(task_offer.feedback, 'feedback')

        # Checks other output data
        self.assertIsNotNone(response.context['total_budget'])
        self.assertIsNotNone(response.context['status_form'])
        self.assertIsNotNone(response.context['offer_response_form'])


class GiveTaskOfferBoundaryTest(unittest.TestCase):
    chars_200 = 'NNEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNNFRLRGi2'\
        'KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXC'\
        'hNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNue'

    chars_500 = 'NNEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNNFRLRGi2KoOtEcOLJyGW'\
        'wOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPGWwOaXChNueq2bq12qrlS6S0YPHkku3JsfYo2BPWOhkNNFRLRGi2'\
        'KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyEcOLJyGWwOaXChNue'\
        'GWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueNNEcOLJyGWwOaXC'\
        'hNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3Js'\
        'fYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOt'

    def setUp(self):
        self.client = Client()
        create_multiple_test_data()
        self.project_id = Project.objects.get(title="Project2").pk
        self.task = Task.objects.get(title="Task3")
        self.nr_of_offers = TaskOffer.objects.count()

        self.chars_200 = 'NNEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNNFRLRGi2'\
            'KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXC'\
            'hNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNue'
        self.chars_500 = 'NNEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNNFRLRGi2KoOtEcOLJyGW'\
            'wOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPGWwOaXChNueq2bq12qrlS6S0YPHkku3JsfYo2BPWOhkNNFRLRGi2'\
            'KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyEcOLJyGWwOaXChNue'\
            'GWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueNNEcOLJyGWwOaXC'\
            'hNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3Js'\
            'fYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOt'

        self.client.login(username='User3', password='top_secret')
        self.offer_test_data = {
            'title': "TaskOffer5",
            'description': 'description',
            'price': 123,
            'taskvalue': self.task.id,
            'offer_submit': True
        }

    def tearDown(self):
        delete_test_data()

    def test_give_offer_title_0(self):
        self.offer_test_data['title'] = ''
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers)

    def test_give_offer_title_1(self):
        self.offer_test_data['title'] = 'A'
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers + 1)

    def test_give_offer_title_200(self):
        self.offer_test_data['title'] = self.chars_200
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers + 1)

    def test_give_offer_title_201(self):
        self.offer_test_data['title'] = self.chars_200 + 'A'
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers)

    def test_give_offer_description_0(self):
        self.offer_test_data['description'] = ''
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers)

    def test_give_offer_description_1(self):
        self.offer_test_data['description'] = 'A'
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers + 1)

    def test_give_offer_description_500(self):
        self.offer_test_data['description'] = self.chars_500
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers + 1)

    def test_give_offer_description_501(self):
        self.offer_test_data['description'] = self.chars_500 + 'A'
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers)

    @unittest.skip("Should fail - doesn't")
    def test_give_offer_price_minus_1(self):
        self.offer_test_data['price'] = -1
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers)

    def test_give_offer_price_0(self):
        self.offer_test_data['price'] = 0
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers + 1)

    def test_give_offer_price_1(self):
        self.offer_test_data['price'] = 1
        self.client.post(f'/projects/{self.project_id}/', self.offer_test_data)
        self.assertEqual(TaskOffer.objects.count(), self.nr_of_offers + 1)
