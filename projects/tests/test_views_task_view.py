import unittest
from django.test import Client

from projects.models import (Project, Task, TaskOffer, Delivery, Team)

from test_helpers import (
    delete_test_data,
    create_multiple_test_data,
    create_delivery)


class TaskViewRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()
        self.project_id = Project.objects.get(title="Project1").pk
        self.task = Task.objects.get(title="Task2")

    def tearDown(self):
        delete_test_data()

    def test_post_delivery(self):
        self.client.login(username='User2', password='top_secret')
        with open('projects/tests/test_file.txt') as test_file:
            response = self.client.post(
                f'/projects/{self.project_id}/tasks/{self.task.id}/',
                {
                    'delivery': True,
                    'comment': 'Comment',
                    'file': test_file},
                follow=True)

            delivery = response.context['deliveries'][0]
            self.assertEquals(delivery.comment, "Comment")
            self.assertTrue("test_file" in delivery.file.path)
            self.assertEquals(delivery.delivery_user.user.username, "User2")

    def test_post_delivery_response(self):
        create_delivery()
        delivery = Delivery.objects.get(task=self.task)
        self.client.login(username='User1', password='top_secret')
        response = self.client.post(
            f'/projects/{self.project_id}/tasks/{self.task.id}/',
            {
                'delivery-response': True,
                'delivery-id': delivery.id,
                'status': 'a',
                'feedback': 'Feedback',
            })

        delivery = response.context['deliveries'][0]
        self.assertEquals(delivery.responding_user.user.username, "User1")
        self.assertEquals(response.context['task'].status, "pp")

    def test_post_team(self):
        self.client.login(username='User2', password='top_secret')
        self.client.post(f'/projects/{self.project_id}/tasks/{self.task.id}/', {
            'team': True,
            'name': 'Name'
        })

        team = Team.objects.get(name="Name")
        self.assertEquals(team.task, self.task)
