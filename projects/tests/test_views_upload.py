import unittest
from django.test import Client
from projects.models import Project, Task, TaskFile

from test_helpers import create_multiple_test_data, delete_test_data


class FileUploadRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()

    def tearDown(self):
        delete_test_data()

    def test_task_file_upload(self):
        project_id = Project.objects.get(title="Project1").pk
        task_id = Task.objects.get(title="Task1").id
        self.client.login(username='User1', password='top_secret')
        with open('projects/tests/test_file.txt') as test_file:
            self.client.post(f'/projects/{project_id}/tasks/{task_id}/upload/', {
                             'task': task_id, 'file': test_file}, follow=True)

            task_file = TaskFile.objects.get(task=task_id)
            self.assertEquals(task_file.name(), "test_file.txt")
