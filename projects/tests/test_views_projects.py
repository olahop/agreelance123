import unittest
from django.test import Client

from test_helpers import create_projects_sorting_data, delete_test_data


class SortingIntegrationTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_projects_sorting_data()
        self.request_data = {
            'sort_form': '',
            'sort_by': 't',
        }

    def tearDown(self):
        delete_test_data()

    def test_sort_by_title_asc(self):
        response = self.client.post('/projects/', self.request_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['projects'][0].title, "A Project")
        self.assertEqual(response.context['projects'][1].title, "B Project")
        self.assertEqual(response.context['projects'][2].title, "C Project")

    def test_sort_by_title_desc(self):
        self.request_data['sort_by'] = '!t'
        response = self.client.post('/projects/', self.request_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['projects'][0].title, "C Project")
        self.assertEqual(response.context['projects'][1].title, "B Project")
        self.assertEqual(response.context['projects'][2].title, "A Project")

    def test_sort_by_owner_asc(self):
        self.request_data['sort_by'] = 'o'
        response = self.client.post('/projects/', self.request_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.context['projects'][0].user), "A User")
        self.assertEqual(str(response.context['projects'][1].user), "B User")
        self.assertEqual(str(response.context['projects'][2].user), "C User")

    def test_sort_by_owner_desc(self):
        self.request_data['sort_by'] = '!o'
        response = self.client.post('/projects/', self.request_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.context['projects'][0].user), "C User")
        self.assertEqual(str(response.context['projects'][1].user), "B User")
        self.assertEqual(str(response.context['projects'][2].user), "A User")
