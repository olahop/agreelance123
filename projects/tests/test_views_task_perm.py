import unittest
from django.test import Client

from user.models import User
from projects.models import Project, Task

from test_helpers import create_multiple_test_data, delete_test_data


class TaskPermissionsRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()
        self.project = Project.objects.get(title="Project1")
        self.task = Task.objects.get(title="Task1")

    def tearDown(self):
        delete_test_data()

    def test_unauthorized_route(self):
        self.client.login(username='User2', password='top_secret')
        response = self.client.post(
            f'/projects/{self.project.pk}/tasks/{self.task.id}/permissions/',
            follow=True)
        self.assertTrue(
            (f'/projects/{self.project.pk}/tasks/{self.task.id}/',
             302) in response.redirect_chain)

    def test_valid_perm_form_route(self):
        self.client.login(username='User1', password='top_secret')
        added_user = User.objects.get(username="User2")
        response = self.client.post(
            f'/projects/{self.project.pk}/tasks/{self.task.id}/permissions/', {
                'user': added_user.id, 'permission': 'Write'}, follow=True)

        self.assertTrue(added_user.profile in self.task.write.all())
        self.assertTrue(
            (f'/projects/{self.project.pk}/tasks/{self.task.id}/',
             302) in response.redirect_chain)

    def test_unvalid_perm_form_route(self):
        self.client.login(username='User1', password='top_secret')
        response = self.client.post(
            f'/projects/{self.project.pk}/tasks/{self.task.id}/permissions/',
            follow=True)
        self.assertIsNotNone(response.context['project'])
