import unittest
from django.test import Client
from projects.models import Project, ProjectCategory, Task

from test_helpers import create_test_user, delete_test_data


class NewProjectRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.user = create_test_user()
        category = ProjectCategory.objects.create(name='Category')
        self.request_data = {
            'title': 'Project1',
            'description': 'Description',
            'category_id': category.id,
            'task_title': 'Task1',
            'task_budget': 123,
            'task_description': 'TaskDescription'
        }

    def tearDown(self):
        delete_test_data()

    def test_views_new_project(self):
        self.client.login(username='User', password='top_secret')
        response = self.client.post(
            '/projects/new/',
            self.request_data,
            follow=True)

        new_project_id = int(response.redirect_chain[0][0].split("/")[2])
        new_project = Project.objects.get(pk=new_project_id)
        self.assertEquals(new_project.user.user, self.user)
        self.assertEquals(new_project.category.name, "Category")

        new_task = Task.objects.get(title="Task1")
        self.assertEquals(new_task.description, "TaskDescription")
        self.assertEquals(new_task.budget, 123)
        self.assertEquals(new_task.project, new_project)
