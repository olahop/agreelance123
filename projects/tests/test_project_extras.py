import unittest
from django.test import Client

from user.models import User
from projects.models import Task

from projects.templatetags.project_extras import get_user_task_permissions

from test_helpers import create_multiple_test_data, delete_test_data


class UserTaskPermissionStatementTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()
        self.task = Task.objects.get(title="Task2")

    def tearDown(self):
        delete_test_data()

    def test_owner_task_permissions(self):
        user = User.objects.get(username="User1")
        get_user_task_permissions(user, self.task)

    def test_unrelated_task_permissions(self):
        user = User.objects.get(username="User3")
        get_user_task_permissions(user, self.task)

    def test_offerer_task_permissions(self):
        user = User.objects.get(username="User2")
        get_user_task_permissions(user, self.task)
