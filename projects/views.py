from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import (
    Project,
    Task,
    TaskFile,
    ProjectCategory)
from .forms import (
    ProjectForm,
    TaskFileForm,
    ProjectStatusForm,
    TaskOfferForm,
    TaskOfferResponseForm,
    TaskPermissionForm,
    DeliveryForm,
    TaskDeliveryResponseForm,
    TeamForm,
    TeamAddForm,
    SortForm)

from .templatetags.project_extras import get_user_task_permissions
from .templatetags.projects_extras import sort_projects
from .templatetags.new_project_extras import create_new_project
from .templatetags.project_view_extras import (
    offer_response_handler,
    edit_project_handler,
    offer_submit_handler,
    edit_offer_handler)
from .templatetags.upload_extras import file_upload_and_access_handler
from .templatetags.task_permissions_extras import (
    can_modify_permissions, change_permissions)
from .templatetags.task_view_extras import (
    delivery_handler,
    delivery_response_handler,
    team_handler,
    team_add_handler,
    permissions_handler)


def projects(request):

    all_projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    sort_form = SortForm()

    if request.method == 'POST' and 'sort_form' in request.POST:
        sort_form = SortForm(request.POST)
        if sort_form.is_valid():
            sort_form.save(commit=False)
            all_projects = sort_projects(all_projects, request.POST.get('sort_by'))

    return render(request,
                  'projects/projects.html',
                  {
                      'projects': all_projects,
                      'project_categories': project_categories,
                      'sort_form': sort_form,
                  })


@login_required
def new_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            create_new_project(request, project)
            return redirect('project_view', project_id=project.id)
    else:
        form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form})


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = 0  # Initializes the total budget to 0

    for item in tasks:
        total_budget += item.budget

    return_context = {
        'project': project,
        'tasks': tasks,
        'total_budget': total_budget,
    }

    if request.user == project.user.user:
        if request.method == 'POST' and 'offer_response' in request.POST:
            offer_response_handler(request)
        return_context['offer_response_form'] = TaskOfferResponseForm()

        if request.method == 'POST' and 'edit_project' in request.POST:
            edit_project_handler(request, project, tasks)

        if request.method == 'POST' and 'status_change' in request.POST:
            status_form = ProjectStatusForm(request.POST)
            if status_form.is_valid():
                project_status = status_form.save(commit=False)
                project.status = project_status.status
                project.save()
        return_context['status_form'] = ProjectStatusForm(
            initial={'status': project.status})

    else:
        if request.method == 'POST' and 'offer_submit' in request.POST:
            offer_submit_handler(request)
        return_context['task_offer_form'] = TaskOfferForm()

        if request.method == 'POST' and 'edit_offer' in request.POST:
            edit_offer_handler(request)

    return render(request, 'projects/project_view.html', return_context)


@login_required
def upload_file_to_task(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)
    user_permissions = get_user_task_permissions(request.user, task)

    if user_permissions['upload']:
        project = Project.objects.get(pk=project_id)
        if request.method == 'POST':
            task_file_form = TaskFileForm(request.POST, request.FILES)
            if task_file_form.is_valid():
                file_upload_and_access_handler(
                    request, project, task, user_permissions)
                return redirect(
                    'task_view',
                    project_id=project_id,
                    task_id=task_id)

        return render(
            request,
            'projects/upload_file_to_task.html',
            {
                'project': project,
                'task': task,
                'task_file_form': TaskFileForm(),
            }
        )
    return redirect('/user/login')  # Redirects to /user/login


@login_required
def task_view(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)
    user_permissions = get_user_task_permissions(request.user, task)

    if (user_permissions['read'] or
            user_permissions['write'] or
            user_permissions['modify'] or
            user_permissions['owner'] or
            user_permissions['view_task']):
        if request.method == 'POST':
            accepted_task_offer = task.accepted_task_offer()
            if accepted_task_offer and accepted_task_offer.offerer == request.user.profile:
                if 'delivery' in request.POST:
                    delivery_handler(request, task)
                if 'team' in request.POST:
                    team_handler(request, task)
                if 'team-add' in request.POST:
                    team_add_handler(request)
                if 'permissions' in request.POST:
                    permissions_handler(request, task)
            if 'delivery-response' in request.POST:
                delivery_response_handler(request, task)

        team_files = []
        per = {}
        for task_file in task.files.all():
            per[task_file.name()] = {}
            for task_team in task_file.teams.all():
                per[task_file.name()][task_team.team.name] = task_team
                if task_team.read:
                    team_files.append(task_team)
        return render(request, 'projects/task_view.html', {
            'task': task,
            'project': Project.objects.get(pk=project_id),
            'user_permissions': user_permissions,
            'deliver_form': DeliveryForm(),
            'deliveries': task.delivery.all(),
            'deliver_response_form': TaskDeliveryResponseForm(),
            'team_form': TeamForm(),
            'team_add_form': TeamAddForm(),
            'team_files': team_files,
            'per': per
        })

    return redirect('/user/login')


@login_required
def task_permissions(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    if (can_modify_permissions(request, project, task) and
            int(project_id) == task.project.id and
            request.method == 'POST'):

        task_permission_form = TaskPermissionForm(request.POST)
        if task_permission_form.is_valid():
            change_permissions(task, task_permission_form)
        else:
            return render(
                request,
                'projects/task_permissions.html',
                {
                    'project': project,
                    'task': task,
                    'form': TaskPermissionForm(),
                }
            )
    return redirect('task_view', project_id=project_id, task_id=task_id)


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
