from django.shortcuts import get_object_or_404

from projects.models import Task, TaskOffer
from projects.forms import (
    EditTaskOfferForm,
    EditProjectForm,
    TaskOfferForm,
    TaskOfferResponseForm)


def offer_response_handler(request):
    instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
    offer_response_form = TaskOfferResponseForm(
        request.POST, instance=instance)
    if offer_response_form.is_valid():
        offer_response = offer_response_form.save(commit=False)

        if offer_response.status == 'a':
            offer_response.task.read.add(offer_response.offerer)
            offer_response.task.write.add(offer_response.offerer)
            project = offer_response.task.project
            project.participants.add(offer_response.offerer)

        offer_response.save()


def edit_project_handler(request, project, tasks):
    edit_project_form = EditProjectForm(request.POST)
    if edit_project_form.is_valid() and project.status == 'o':
        if project.edit:
            project.description = request.POST['project_description']
            for task in tasks:
                task.budget = request.POST[f'task{task.id}_budget']
                task.description = request.POST[f'task{task.id}_description']
                task.save()
        project.edit = not project.edit
        project.save()


def offer_submit_handler(request):
    task_offer_form = TaskOfferForm(request.POST)
    if task_offer_form.is_valid():
        task_offer = task_offer_form.save(commit=False)
        task_offer.task = Task.objects.get(pk=request.POST.get('taskvalue'))
        task_offer.offerer = request.user.profile
        task_offer.save()


def edit_offer_handler(request):
    edit_task_offer_form = EditTaskOfferForm(request.POST)
    if edit_task_offer_form.is_valid():
        offer = TaskOffer.objects.get(id=request.POST.get('offer_id'))
        if offer.status == 'p' and request.user == offer.offerer.user:
            if offer.edit:
                offer.description = request.POST['offer_description']
                offer.price = request.POST['offer_price']
            offer.edit = not offer.edit
            offer.save()


"""
def status_change_handler(request, project):
    status_form = ProjectStatusForm(request.POST)
    if status_form.is_valid():
        project_status = status_form.save(commit=False)
        project.status = project_status.status
        project.save()
"""
