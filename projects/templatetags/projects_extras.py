
def get_username(elem):
    return elem.user.user.username.lower()


def get_title(elem):
    return elem.title.lower()


def get_timestamp(elem):
    return elem.timestamp


def sort_projects(projects, sort_by):
    if sort_by == 't':
        projects = sorted(projects, key=get_title)
    elif sort_by == '!t':
        projects = sorted(projects, key=get_title, reverse=True)
    elif sort_by == 'o':
        projects = sorted(projects, key=get_username)
    elif sort_by == '!o':
        projects = sorted(projects, key=get_username, reverse=True)
    elif sort_by == 'd':
        projects = sorted(projects, key=get_timestamp)
    elif sort_by == '!d':
        projects = sorted(projects, key=get_timestamp, reverse=True)
    return projects
