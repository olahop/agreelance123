from django.shortcuts import get_object_or_404
from django.utils import timezone

from projects.models import Delivery, Team, TaskFileTeam
from projects.forms import (
    TaskDeliveryResponseForm,
    DeliveryForm,
    TeamForm,
    TeamAddForm)


def delivery_handler(request, task):
    deliver_form = DeliveryForm(request.POST, request.FILES)
    if deliver_form.is_valid():
        delivery = deliver_form.save(commit=False)
        delivery.task = task
        delivery.delivery_user = request.user.profile
        delivery.save()
        task.status = "pa"
        task.save()


def delivery_response_handler(request, task):
    instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id'))
    deliver_response_form = TaskDeliveryResponseForm(
        request.POST, instance=instance)
    if deliver_response_form.is_valid():
        delivery = deliver_response_form.save()
        delivery.responding_time = timezone.now()
        delivery.responding_user = request.user.profile
        delivery.save()

        if delivery.status == 'a':
            task.status = "pp"
            task.save()
        elif delivery.status == 'd':
            task.status = "dd"
            task.save()


def team_handler(request, task):
    team_form = TeamForm(request.POST)
    if (team_form.is_valid()):
        team = team_form.save(False)
        team.task = task
        team.save()


def team_add_handler(request):
    instance = get_object_or_404(Team, id=request.POST.get('team-id'))
    team_add_form = TeamAddForm(request.POST, instance=instance)
    if team_add_form.is_valid():
        team = team_add_form.save(False)
        team.members.add(*team_add_form.cleaned_data['members'])
        team.save()


def permissions_handler(request, task):
    for team in task.teams.all():
        for task_file in task.files.all():
            try:
                tft_string = 'permission-perobj-' + \
                    str(task_file.id) + '-' + str(team.id)
                tft_id = request.POST.get(tft_string)
                instance = TaskFileTeam.objects.get(id=tft_id)
            except Exception as e:
                instance = TaskFileTeam(
                    file=task_file,
                    team=team,
                )

            instance.read = request.POST.get(
                'permission-read-' + str(task_file.id) + '-' + str(team.id)) or False
            instance.write = request.POST.get(
                'permission-write-' + str(task_file.id) + '-' + str(team.id)) or False
            instance.modify = request.POST.get(
                'permission-modify-' + str(task_file.id) + '-' + str(team.id)) or False
            instance.save()
        team.write = request.POST.get(
            'permission-upload-' + str(team.id)) or False
        team.save()
