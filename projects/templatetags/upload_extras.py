from django.contrib import messages

from projects.models import TaskFileTeam, directory_path
from projects.forms import TaskFileForm


def file_upload_and_access_handler(request, project, task, user_permissions):
    accepted_task_offer = task.accepted_task_offer()
    task_file_form = TaskFileForm(request.POST, request.FILES)
    task_file = task_file_form.save(commit=False)
    task_file.task = task
    existing_file = task.files.filter(
        file=directory_path(
            task_file,
            task_file.file.file)).first()
    access = user_permissions['modify']
    for team in request.user.profile.teams.all():
        file_modify_access = TaskFileTeam.objects.filter(
            team=team, file=existing_file, modify=True).exists()
        access = access or file_modify_access
    if access:
        if existing_file:
            existing_file.delete()
        task_file.save()
        if request.user.profile != project.user and request.user.profile != accepted_task_offer.offerer:
            teams = request.user.profile.teams.filter(task__id=task.id)
            for team in teams:
                tft = TaskFileTeam()
                tft.team = team
                tft.file = task_file
                tft.read = True
                tft.save()
    else:
        messages.warning(request, "You do not have access to modify this file")
