from django import template
from django.contrib.auth.models import User
from ..models import TaskOffer

register = template.Library()


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key) if dictionary else dictionary


@register.filter
def read(per):
    return per.read if per else 0


@register.filter
def write(per):
    return per.write if per else 0


@register.filter
def modify(per):
    return per.modify if per else 0


@register.filter
def id(per):
    return per.id if per else None


@register.simple_tag
def define(val=None):
    return val


@register.filter
def check_taskoffers(task, user):
    taskoffers = task.taskoffer_set.filter(offerer=user.profile)
    useroffers = []

    for item in taskoffers:
        useroffers.append(item)

    return useroffers


@register.filter
def get_all_taskoffers(value):
    taskoffers = value.taskoffer_set.all()
    return taskoffers


@register.filter
def get_accepted_task_offer(task):
    task_offer = None
    try:
        task_offer = task.taskoffer_set.get(status='a')
    except TaskOffer.DoesNotExist:
        pass

    return task_offer


@register.filter
def get_project_participants_string(project):

    participants_string = ', '.join(get_project_participants(project))

    return participants_string


def get_project_participants(project):
    query = project.participants.all()
    participants = set()
    for participant in query:
        participants.add(participant.user.username)

    return participants


def get_user_task_permissions(user, task):

    if user == task.project.user.user:
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }
    if task.accepted_task_offer() and task.accepted_task_offer().offerer == user.profile:
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }
    user_permissions = {
        'write': False,
        'read': False,
        'modify': False,
        'owner': False,
        'view_task': False,
        'upload': False,
    }
    user_permissions['read'] = user_permissions['read'] or user.profile.task_participants_read.filter(
        id=task.id).exists()

    # Team members can view its teams tasks
    user_permissions['upload'] = user_permissions['upload'] or user.profile.teams.filter(
        task__id=task.id, write=True).exists()
    user_permissions['view_task'] = user_permissions['view_task'] or user.profile.teams.filter(
        task__id=task.id).exists()

    user_permissions['write'] = user_permissions['write'] or user.profile.task_participants_write.filter(
        id=task.id).exists()
    user_permissions['modify'] = user_permissions['modify'] or user.profile.task_participants_modify.filter(
        id=task.id).exists()

    return user_permissions
