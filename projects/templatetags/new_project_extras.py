from user.models import Profile
from projects.models import Task, ProjectCategory
from django.core import mail
from django.shortcuts import get_object_or_404
from django.contrib.sites.shortcuts import get_current_site
from django.contrib import messages


def create_new_project(request, project):
    project.user = request.user.profile
    project.category = get_object_or_404(
        ProjectCategory, id=request.POST.get('category_id'))
    project.save()
    people = Profile.objects.filter(categories__id=project.category.id)
    for person in people:
        if person.user.email:
            mail_person_in_people(request, project, person)
    task_title = request.POST.getlist('task_title')
    task_description = request.POST.getlist('task_description')
    task_budget = request.POST.getlist('task_budget')
    for i in range(0, len(task_title)):
        Task.objects.create(
            title=task_title[i],
            description=task_description[i],
            budget=task_budget[i],
            project=project,
        )


def mail_person_in_people(request, project, person):
    current_site = get_current_site(request)
    try:
        with mail.get_connection() as connection:
            mail.EmailMessage(
                "New Project: " + project.title,
                "A new project you might be interested in was created and can be" +
                " viewed at " + current_site.domain + '/projects/' + str(project.id),
                "Agreelancer",
                [person.user.email],
                connection=connection,
            ).send()
    except Exception as e:
        messages.success(
            request,
            'Sending of email to ' +
            person.user.email +
            " failed: " +
            str(e))
