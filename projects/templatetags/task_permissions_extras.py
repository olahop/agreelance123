from user.models import User


def can_modify_permissions(request, project, task):
    accepted_task_offer = task.accepted_task_offer()
    return (project.user == request.user.profile or (
        accepted_task_offer and request.user == accepted_task_offer.offerer.user))


def change_permissions(task, task_permission_form):
    try:
        username = task_permission_form.cleaned_data['user']
        user = User.objects.get(username=username)
        permission_type = task_permission_form.cleaned_data['permission']
        if permission_type == 'Read':
            task.read.add(user.profile)
        elif permission_type == 'Write':
            task.write.add(user.profile)
        elif permission_type == 'Modify':
            task.modify.add(user.profile)
    except (Exception):
        print("user not found")
