from django.test import Client

from django.contrib.auth.models import User

from user.models import Profile
from projects.models import (
    Projects,
    Project,
    ProjectCategory,
    Task,
    TaskOffer,
    Delivery,
    TaskFile,
    TaskFileTeam,
    Team)
from payment.models import Payment


def create_test_user():
    return User.objects.create_user(
        username='User',
        email='test@test.com',
        password='top_secret'
    )


def create_task_status_data():
    create_multiple_test_data()
    Task.objects.create(
        project=Project.objects.get(title="Project1"),
        title='Task5',
        description='',
        budget=20,
        status='pa',
        feedback=''
    )
    Task.objects.create(
        project=Project.objects.get(title="Project1"),
        title='Task6',
        description='',
        budget=20,
        status='pp',
        feedback=''
    )


def create_projects_sorting_data():
    test_user_a = User.objects.create_user(
        username='A User',
        email='test@test.com',
        password='top_secret'
    )
    test_user_b = User.objects.create_user(
        username='B User',
        email='test@test.com',
        password='top_secret'
    )
    test_user_c = User.objects.create_user(
        username='C User',
        email='test@test.com',
        password='top_secret'
    )
    test_category = ProjectCategory.objects.create(name='Category')

    test_project_a = Project.objects.create(
        user=test_user_c.profile,
        title='A Project',
        description='',
        category=test_category,
        status='o'
    )
    test_project_b = Project.objects.create(
        user=test_user_a.profile,
        title='B Project',
        description='',
        category=test_category,
        status='o'
    )
    test_project_c = Project.objects.create(
        user=test_user_b.profile,
        title='C Project',
        description='',
        category=test_category,
        status='o'
    )


def create_multiple_test_data():
    test_user_1 = User.objects.create_user(
        username='User1',
        email='test@test.com',
        password='top_secret'
    )
    test_user_2 = User.objects.create_user(
        username='User2',
        email='test@test.com',
        password='top_secret'
    )
    test_user_3 = User.objects.create_user(
        username='User3',
        email='test@test.com',
        password='top_secret'
    )

    test_category_1 = ProjectCategory.objects.create(name='Category1')
    test_category_2 = ProjectCategory.objects.create(name='Category2')
    test_category_3 = ProjectCategory.objects.create(name='Category3')

    test_project_1 = Project.objects.create(
        user=test_user_1.profile,
        title="Project1",
        description='',
        category=test_category_1,
        status='o'
    )
    test_project_2 = Project.objects.create(
        user=test_user_2.profile,
        title="Project2",
        description='',
        category=test_category_2,
        status='o',
        edit=True
    )

    test_task_1 = Task.objects.create(
        project=test_project_1,
        title='Task1',
        description='',
        budget=20,
        status='ad',
        feedback=''
    )
    test_task_2 = Task.objects.create(
        project=test_project_1,
        title='Task2',
        description='',
        budget=20,
        status='ps',
        feedback=''
    )
    test_task_3 = Task.objects.create(
        project=test_project_2,
        title='Task3',
        description='',
        budget=20,
        status='ad',
        feedback=''
    )
    test_task_4 = Task.objects.create(
        project=test_project_2,
        title='Task4',
        description='',
        budget=20,
        status='ad',
        feedback=''
    )

    test_taskoffer_1 = TaskOffer.objects.create(
        task=test_task_1,
        title='TaskOffer1',
        description='',
        price=20,
        offerer=test_user_2.profile,
    )
    test_taskoffer_2 = TaskOffer.objects.create(
        task=test_task_2,
        title='TaskOffer2',
        description='',
        price=20,
        offerer=test_user_2.profile,
        status='a'
    )
    test_taskoffer_3 = TaskOffer.objects.create(
        task=test_task_3,
        title='TaskOffer3',
        description='',
        price=20,
        offerer=test_user_1.profile,
        edit=True
    )
    test_taskoffer_4 = TaskOffer.objects.create(
        task=test_task_4,
        title='TaskOffer4',
        description='',
        price=20,
        offerer=test_user_1.profile,
    )


def create_delivery():
    client = Client()
    project_id = Project.objects.get(title="Project1").pk
    task_id = Task.objects.get(title="Task2").id
    client.login(username='User2', password='top_secret')
    with open('projects/tests/test_file.txt') as test_file:
        client.post(f'/projects/{project_id}/tasks/{task_id}/', {
            'delivery': True,
            'comment': 'Comment',
            'file': test_file
        })


def delete_test_data():
    User.objects.all().delete()
    Profile.objects.all().delete()

    ProjectCategory.objects.all().delete()
    Projects.objects.all().delete()
    Project.objects.all().delete()

    Task.objects.all().delete()
    TaskOffer.objects.all().delete()
    TaskFile
    TaskFileTeam

    Team
    Delivery.objects.all().delete()
    Payment.objects.all().delete()
