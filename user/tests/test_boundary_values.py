import unittest
from django.test import Client
from django.contrib.auth.models import User

from projects.models import Project, ProjectCategory

from test_helpers import delete_test_data

chars_30 = "NNEcOLJyGWwOaXChNueq2sfYo2BPWO"
chars_50 = "NNEcOLJyGWwOaXueq2sfYo2BPWOyGWwOaXChNueq2sfYo2BPWO"
chars_150 = "kNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3Js"\
    "fYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3J"\
    "sfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNue"
email_254 = "kNFkjhRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3"\
    "JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12kNFRLRGi2KoOtE"\
    "cOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEc"\
    "OLJyGWwOaXChNueq2bq12qrlS6qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2Ko"\
    "OtEcOLJyG@gmail.com"


class SignUpUsernameBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_username_boundary_0(self):
        self.test_data['username'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_username_boundary_1(self):
        self.test_data['username'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_username_boundary_150(self):
        self.test_data['username'] = chars_150
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_username_boundary_151(self):
        self.test_data['username'] = chars_150 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpFirstNameBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_first_name_boundary_0(self):
        self.test_data['first_name'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_first_name_boundary_1(self):
        self.test_data['first_name'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_first_name_boundary_150(self):
        self.test_data['first_name'] = chars_30
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_first_name_boundary_151(self):
        self.test_data['first_name'] = chars_30 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpLastNameBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_last_name_boundary_0(self):
        self.test_data['last_name'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_last_name_boundary_1(self):
        self.test_data['last_name'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_last_name_boundary_150(self):
        self.test_data['last_name'] = chars_30
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_last_name_boundary_151(self):
        self.test_data['last_name'] = chars_30 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpCategoriesBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        self.category_1 = ProjectCategory.objects.create(name='Category')

        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': self.category_1.id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_categories_boundary_0(self):
        self.test_data['categories'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_categories_boundary_multiple(self):
        ProjectCategory.objects.create(name='Category2')
        category_2_id = ProjectCategory.objects.get(name="Category2").id
        test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': self.category_1.id,
            'categories': category_2_id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }
        self.client.post('/user/signup/', test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)


class SignUpCompanyBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_company_boundary_30(self):
        self.test_data['company'] = chars_30
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_company_boundary_31(self):
        self.test_data['company'] = chars_30 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpEmailBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_email_boundary_254(self):
        self.test_data['email'] = email_254
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_email_boundary_255(self):
        self.test_data['email'] = "A" + email_254
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpPasswordBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    @unittest.skip("Should not add user - does")
    def test_password_boundary_7(self):
        self.test_data['password'] = 'invalid'
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_password_boundary_8(self):
        self.test_data['password'] = "!invalid"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)


class SignUpPhoneNumberBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_phone_number_boundary_0(self):
        self.test_data['phone_number'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_phone_number_boundary_1(self):
        self.test_data['phone_number'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_phone_number_boundary_50(self):
        self.test_data['phone_number'] = chars_50
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_phone_number_boundary_51(self):
        self.test_data['phone_number'] = chars_50 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpCountryBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_country_boundary_0(self):
        self.test_data['country'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_country_boundary_1(self):
        self.test_data['country'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_country_boundary_50(self):
        self.test_data['country'] = chars_50
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_country_boundary_51(self):
        self.test_data['country'] = chars_50 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpStateBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_state_boundary_0(self):
        self.test_data['state'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_state_boundary_1(self):
        self.test_data['state'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_state_boundary_50(self):
        self.test_data['state'] = chars_50
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_state_boundary_51(self):
        self.test_data['state'] = chars_50 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpCityBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_city_boundary_0(self):
        self.test_data['city'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_city_boundary_1(self):
        self.test_data['city'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_city_boundary_50(self):
        self.test_data['city'] = chars_50
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_city_boundary_51(self):
        self.test_data['city'] = chars_50 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpPostalCodeBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_postal_code_boundary_0(self):
        self.test_data['postal_code'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_postal_code_boundary_1(self):
        self.test_data['postal_code'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_postal_code_boundary_50(self):
        self.test_data['postal_code'] = chars_50
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_postal_code_boundary_51(self):
        self.test_data['postal_code'] = chars_50 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)


class SignUpStreetAddressBoundaryTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': ProjectCategory.objects.get(name="Category").id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_street_address_boundary_0(self):
        self.test_data['street_address'] = ''
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)

    def test_street_address_boundary_1(self):
        self.test_data['street_address'] = "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_street_address_boundary_50(self):
        self.test_data['street_address'] = chars_50
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users + 1)

    def test_street_address_boundary_51(self):
        self.test_data['street_address'] = chars_50 + "A"
        self.client.post('/user/signup/', self.test_data)
        self.assertEquals(User.objects.count(), self.nr_of_users)
