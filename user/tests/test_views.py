import unittest
from django.test import Client
from django.contrib.auth.models import User

from projects.models import ProjectCategory

from test_helpers import delete_test_data


class SignUpRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.nr_of_users = User.objects.count()

        category = ProjectCategory.objects.create(name='Category')
        self.test_data = {
            'username': 'username',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'categories': category.id,
            'company': 'company',
            'email': 'user@example.com',
            'email_confirmation': 'user@example.com',
            'password1': 'veryDifficult123',
            'password2': 'veryDifficult123',
            'phone_number': '987654321',
            'country': 'Country',
            'state': 'State',
            'city': 'City',
            'postal_code': '1234',
            'street_address': 'Street 1'
        }

    def tearDown(self):
        delete_test_data()

    def test_signup_post(self):
        response = self.client.post(
            '/user/signup/', self.test_data, follow=True)
        self.assertTrue(('/', 302) in response.redirect_chain)

    def test_signup_get(self):
        response = self.client.get('/user/signup/')
        self.assertIsNotNone(response.context['form'])
