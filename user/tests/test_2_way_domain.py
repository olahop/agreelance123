import unittest
from django.test import Client
from django.contrib.auth.models import User

from projects.models import ProjectCategory

from test_helpers import delete_test_data

chars_30 = "NNEcOLJyGWwOaXChNueq2sfYo2BPWO"
chars_50 = "NNEcOLJyGWwOaXueq2sfYo2BPWOyGWwOaXChNueq2sfYo2BPWO"
chars_150 = "kNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3Js"\
    "fYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3J"\
    "sfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNue"
email_254 = "kNFkjhRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3"\
    "JsfYo2BPWOhkNFRLRGi2KoOtEcOLJyGWwOaXChNueq2bq12kNFRLRGi2KoOtE"\
    "cOLJyGWwOaXChNueq2bq12qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2KoOtEc"\
    "OLJyGWwOaXChNueq2bq12qrlS6qrlS6S0YPkku3JsfYo2BPWOhkNFRLRGi2Ko"\
    "OtEcOLJyG@gmail.com"


class SignUpDomainTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()

        self.category_1 = ProjectCategory.objects.create(name='Category1')
        self.category_2 = ProjectCategory.objects.create(name='Category2')
        self.category_3 = ProjectCategory.objects.create(name='Category3')

        self.nr_of_users = User.objects.count()

    def tearDown(self):
        delete_test_data()

    def test_sign_up_page_case_1(self):
        self.client.post('/user/signup/', {
            'username': '123',
            'first_name': '123',
            'last_name': '123',
            'categories': self.category_1.id,
            'company': chars_30,
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': '123',
            'country': '123',
            'state': '123',
            'city': '123',
            'postal_code': '123',
            'street_address': '123'
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_2(self):
        self.client.post('/user/signup/', {
            'username': chars_150,
            'first_name': chars_30,
            'last_name': chars_30,
            'categories': self.category_2.id,
            'company': '',
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': chars_50,
            'country': chars_50,
            'state': chars_50,
            'city': chars_50,
            'postal_code': chars_50,
            'street_address': chars_50
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_3(self):
        self.client.post('/user/signup/', {
            'username': chars_150,
            'first_name': chars_30,
            'last_name': chars_30,
            'categories': self.category_3.id,
            'company': chars_30,
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': chars_50,
            'country': chars_50,
            'state': '123',
            'city': '123',
            'postal_code': '123',
            'street_address': '123'
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_4(self):
        self.client.post('/user/signup/', {
            'username': '123',
            'first_name': '123',
            'last_name': '123',
            'categories': self.category_3.id,
            'company': '',
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': '123',
            'country': '123',
            'state': chars_50,
            'city': chars_50,
            'postal_code': chars_50,
            'street_address': chars_50
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_5(self):
        self.client.post('/user/signup/', {
            'username': chars_150,
            'first_name': chars_30,
            'last_name': '123',
            'categories': self.category_2.id,
            'company': chars_30,
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': '123',
            'country': '123',
            'state': chars_50,
            'city': chars_50,
            'postal_code': '123',
            'street_address': '123'
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_6(self):
        self.client.post('/user/signup/', {
            'username': '123',
            'first_name': '123',
            'last_name': chars_30,
            'categories': self.category_1.id,
            'company': '',
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': chars_50,
            'country': chars_50,
            'state': '123',
            'city': '123',
            'postal_code': chars_50,
            'street_address': chars_50
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_7(self):
        self.client.post('/user/signup/', {
            'username': '123',
            'first_name': '123',
            'last_name': chars_30,
            'categories': self.category_2.id,
            'company': chars_30,
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': '123',
            'country': '123',
            'state': '123',
            'city': '123',
            'postal_code': chars_50,
            'street_address': '123'
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_8(self):
        self.client.post('/user/signup/', {
            'username': chars_150,
            'first_name': '123',
            'last_name': '123',
            'categories': self.category_1.id,
            'company': chars_30,
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': chars_50,
            'country': '123',
            'state': chars_50,
            'city': '123',
            'postal_code': '123',
            'street_address': chars_50
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)

    def test_sign_up_page_case_9(self):
        self.client.post('/user/signup/', {
            'username': '123',
            'first_name': chars_30,
            'last_name': '123',
            'categories': self.category_1.id,
            'company': '',
            'email': email_254,
            'email_confirmation': email_254,
            'password1': 'Valid123',
            'password2': 'Valid123',
            'phone_number': '123',
            'country': chars_50,
            'state': '123',
            'city': chars_50,
            'postal_code': '123',
            'street_address': '123'
        })
        self.assertEqual(User.objects.count(), self.nr_of_users + 1)
