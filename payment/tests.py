import unittest
from django.test import Client
from django.contrib.auth.models import User

from projects.models import Project, Task
from payment.models import Payment

from test_helpers import delete_test_data, create_multiple_test_data


class PaymentRegressionTest(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        create_multiple_test_data()

    def tearDown(self):
        delete_test_data()

    def test_payment_output(self):
        project = Project.objects.get(title="Project1")
        task = Task.objects.get(title="Task2")

        self.client.login(username='User1', password='top_secret')
        response = self.client.post(
            f'/payment/{project.pk}/{task.id}', follow=True)

        task_payment = Payment.objects.get(task=task)
        self.assertEquals(
            task_payment.payer,
            User.objects.get(
                username="User1").profile)
        self.assertEquals(
            task_payment.receiver,
            User.objects.get(
                username="User2").profile)
        self.assertEquals(Task.objects.get(title="Task2").status, 'ps')
        self.assertTrue(
            (f'/payment/{project.pk}/{task.id}/receipt/',
             302) in response.redirect_chain)
